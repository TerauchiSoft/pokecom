org 100h

t:equ 80h
f:equ 0
grp:equ 0bfd0h
overwrite:equ 0


maze_make:
 ld HL,0
 ld (randp+1),HL
 ld A,(floor)
 ld L,A
 ld H,A
 ld (randpl+1),HL
 ld HL,map_memry_top
 ld (HL),0
 ld DE,map_memry_top+1
 ld BC,95
 ldir
 ld HL,map_memry_left+1
 ld (HL),0
 ld DE,map_memry_left+2
 ld BC,14
 ldir
 ld HL,map_memry_left
 ld BC,17
 ld DE,map_memry_left+17
 ldir
 ld HL,map_memry_left
 ld BC,34
 ld DE,map_memry_left+34
 ldir
 ld HL,map_memry_left
 ld BC,119-68
 ld DE,map_memry_left+68
 ldir

 ld C,1
 ld HL,map_memry_top
 ld DE,map_memry_left
 

























Map_make:
 call cls
 ld HL,Map_data
 ld A,(floor)
; dec A
 ld DE,96+105
 call xsan
 ld DE,map_memry_top
 ld BC,96
 ldir
 ld DE,map_memry_left
 ld A,7
 inc DE
map_make_l:
 push AF
 ld BC,15
 ldir
 inc DE
 inc DE
 pop AF
 dec A
 jr nz,map_make_l
 ld DE,061eh
 ld IX,map_memry_top
 ld IY,map_memry_left

map_make_l_l:
 ld A,(IX)
 and t
 jr z,map_make_Xe
 push DE
 ld HL,graph_wall+0
 call grp_set
 pop DE
map_make_Xe:
 inc IX

 ld A,(IY)
 and t
 jr z,map_make_Ye
 push DE
 ld HL,graph_wall+6
 dec D
 dec D
 dec D
 dec D
 dec D
 dec D
 dec E
 call grp_set
 pop DE
map_make_Ye:
 inc IY

 ld A,7
 add A,E
 ld E,A
 cp 142
 jp nz,map_make_l_l
 
 ld A,7
 add A,D
 ld D,A
 cp 55
 jr z,map_make_end
 ld E,1eh
 inc IY
 jp map_make_l_l
map_make_end:
 ret




 db 0ffh,0ffh,0ffh,0ffh,0ffh,0ffh,0ffh,0ffh,0ffh,0ffh,0ffh,0ffh,0ffh,0ffh,0ffh,0ffh;16
map_memry_top:;1(ture or false)0000000(loop count,7fh=Outer wall)b
 ds 16
 ds 16
 ds 16
 ds 16
 ds 16
 ds 16
 db 0ffh,0ffh,0ffh,0ffh,0ffh,0ffh,0ffh,0ffh,0ffh,0ffh,0ffh,0ffh,0ffh,0ffh,0ffh,0ffh;16



map_memry_left:;1(ture or false)0000000(loop count,7fh=Outer wall)b
 db 0ffh
 ds 15
 db 0ffh
 db 0ffh
 ds 15
 db 0ffh
 db 0ffh
 ds 15
 db 0ffh
 db 0ffh
 ds 15
 db 0ffh
 db 0ffh
 ds 15
 db 0ffh
 db 0ffh
 ds 15
 db 0ffh
 db 0ffh
 ds 15
 db 0ffh

graph_wall:
 db 1,1,1,1,1,1
 db 3fh,0,0,0,0,0
Map_data:;96+105=201byte
 db f,f,t,t,t,f,f,f,f,f,f,t,f,t,f,f
 db f,f,f,t,f,t,f,t,f,t,t,f,t,t,f,t
 db t,f,t,t,f,f,t,f,f,f,f,f,t,f,f,t
 db f,f,t,t,t,t,f,t,f,t,f,f,f,t,f,f
 db f,f,t,t,t,t,f,t,t,f,f,t,f,t,f,t
 db t,f,t,f,f,t,f,f,t,t,f,t,t,t,f,t

 db t,f,f,f,f,f,t,f,t,t,f,f,f,f,t
 db f,t,f,f,t,t,f,t,f,f,f,t,f,t,f
 db t,t,f,t,t,f,f,t,t,t,t,f,t,t,f
 db t,t,f,f,f,t,t,t,f,f,t,t,f,t,f
 db t,f,f,f,t,f,t,f,t,t,t,t,t,f,t
 db f,f,t,f,f,f,f,f,f,t,f,f,t,f,f
 db f,t,f,t,f,t,t,f,f,f,f,f,t,f,f

XSAN:
 OR A
 RET Z
XSANL:
 ADD HL,DE
 DEC A
 JR NZ,XSANL
 RET
floor:db 0
grp_set:;HL_data,DE_(y,x),B_databit
 ld B,6
;

 ld A,D
 and 11000000b
 jr z,grp_set_0
 cp 80h
 jr c,grp_set_1
 jr z,grp_set_2
;left
 ld A,18
 add A,L
 ld L,A
 jr nc,grp_set_0
 inc H
 jr grp_set_0
;dowm
grp_set_2:
 ld A,12
 add A,L
 ld L,A
 jr nc,grp_set_0
 inc H
 jr grp_set_0
;right
grp_set_1:
 ld A,6
 add A,L
 ld L,A
 jr nc,grp_set_0
 inc H

;up,non direction
grp_set_0:
 ld A,E
 ld (grp_set_or1+1),A
 ld A,D
 and 3fh
 ld D,0
grp_set_set1:
 inc D
 sub 8
 jr nc,grp_set_set1
 dec D
 add A,8
grp_set_set2:
 ld (grp_set_shiftd_bit+1),A
 ld C,A
 ld A,8
 sub C
 ld (grp_set_shiftu_bit+1),A

grp_set_shiftd:
 ld A,D			;line number
 push HL					;HL
 ld HL,grapu_area
 ld DE,144
 call xsan		;y+
grp_set_or1:
 ld E,overwrite
 ld D,0
 add HL,DE		;x+
 ex DE,HL
 pop HL			;data
 push HL					;HL
 push BC						;BC
grp_set_shiftd_bit:
 ld C,overwrite		;shift bit count
 ld A,(HL)
 ex AF,AF'
 ld A,C
 or A
 jr z,grp_set_shiftd_bit_2
 ex AF,AF'
grp_set_shiftd_bit_l:
 sla A
 dec C
 jr nz,grp_set_shiftd_bit_l
 ex AF,AF'
grp_set_shiftd_bit_2:
 ex AF,AF'
 ld C,A
 ld A,(DE)
grp_set_change1:
 or C
 ld (DE),A
grp_set_shiftd_l:
 inc HL
 inc DE
 djnz grp_set_shiftd_bit

; dec DE

 ex DE,HL
 ld DE,144
 add HL,DE


 pop BC						;HL
 ld A,L
 sub B
 jr nc,grp_set_shiftu_bit_b
 dec H
grp_set_shiftu_bit_b:
 ld L,A
 ex DE,HL

 pop HL			;data2
grp_set_shiftu_bit:
 ld C,overwrite		;shift bit count
 ld A,(HL)
grp_set_shiftu_bit_l
 srl A
 dec C
 jr nz,grp_set_shiftu_bit_l
 ld C,A
 ld A,(DE)
grp_set_change2:
 or C
 ld (DE),A
grp_set_shiftu_l:
 inc HL
 inc DE
 djnz grp_set_shiftu_bit

 call grp_o
 ret

cls:
 push AF
 push DE
 push HL
 push BC
 ld A,0
 ld HL,grapu_area
 ld DE,grapu_area+1
 ld (HL),A
 ld BC,144*6-1
 ldir
 pop BC
 pop HL
 pop DE
 pop AF
 ret
grp_o:
 ld A,6
 ld DE,0
 ld HL,grapu_area
grp_o_l:
 push DE
 push AF
 ld B,144
 call grp
 inc HL
 pop AF
 pop DE
 inc D
 dec A
 jr nz,grp_o_l

 ret 
 ds 144
grapu_area:
 ds 144*6
 ds 144
RAND:;Aƶ��
 PUSH HL
 push DE
randp:
 LD HL,overwrite
 LD D,H
 LD E,L
 ADD HL,HL
 ADD HL,HL
 ADD HL,DE
randpl:
 LD DE,overwrite
 ADD HL,DE
 LD (RAND+3),HL
 LD A,H
 pop DE
 POP HL
 RET
RAND4:
 CALL RAND
 AND 3
 RET

end