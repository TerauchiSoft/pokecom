      ORG   100H
V0:   DS    7
V1:   DS    151
V2:   DS    158
V3:   DS    158
V4:   DS    158
V5:   DS    158
V6:   DS    158
H1:   DB    144
H2:  DB    144
H3:  DB    144
H4:  DB    144
H5:  DB    144
H6:  DB    144
MAI: PUSH  BC
     PUSH  DE
     PUSH  HL
     LD    A,B
     LD    (BOX),A
     LD    A,D
     ADD   A,10
     LD    D,A
     LD    A,E
     ADD   A,10
     LD    E,A
     LD    (XZA),DE
     LD    DE,UP
     LD    BC,8
     LDIR
     LD    HL,UP
     LD    C,8
     LDIR
     LD    A,(XZA)
     CP    154
     JP    NC,SA
     LD    B,A
     LD    A,(BOX)
     ADD   A,B
     CP    11
     JP    C,SA
     LD    A,(YZA)
     CP    58
     JP    NC,SA
     CP    3
     JR    C,SA
     LD    DE,0
     CP    10
     JR    NC,NE3
     SUB   2
     DEC   D
     JR    NE4
NE3: SUB   10
L1:  CP    8
     JR    C,NE4
     SUB   8
     INC   D
     JR    L1
NE4: LD    E,A
     OR    A
     JR    Z,NE5
     LD    B,E
L2:  PUSH  BC
     LD    B,8
     LD    HL,UP
L3:  SLA   (HL)
     INC   HL
     DJNZ  L3
     POP   BC
     DJNZ  L2
NE5: LD    A,8
     SUB   E
     LD    B,A
L4:  PUSH  BC
     LD    B,8
     LD    HL,DWN
L5:  SRL   (HL)
     INC   HL
     DJNZ  L5
     POP   BC
     DJNZ  L4
     LD    HL,V0
     LD    A,D
     CP    255
     JR    NZ,NE6
     LD    A,(XZA)
     SUB   3
     LD    E,A
     LD    D,0
     ADD   HL,DE
     JR    KK
NE6: OR    A
     JR    Z,NE7
     LD    B,D
     LD    DE,158
L6:  ADD   HL,DE
     DJNZ  L6
NE7: LD    A,(XZA)
     SUB   3
     LD    E,A
    LD    D,0
    ADD   HL,DE
    LD    DE,UP
    PUSH  HL
    CALL  WOW
    POP   HL
    LD    A,(YZA)
    CP    50
    JR    NC,SA
    LD    DE,158
    ADD   HL,DE
KK: LD    DE,DWN
    CALL  WOW
SA: POP   HL
    POP   DE
    POP   BC
    RET
WOW:LD    A,(BOX)
    LD    B,A
L7: LD    A,(DE)
    OR    (HL)
    LD    (HL),A
    INC   DE
    INC   HL
    DJNZ  L7
    RET
BOX:DB    0
XZA:DB    0
YZA:DB    0
UP: DS    8
DWN:DS    8
CLE:LD    HL,V0
    LD    DE,101H
    LD    (HL),0
    LD    BC,947
    LDIR
    RET
DIS:LD    HL,V1
    LD    DE,0
    LD    BC,H1
L8: PUSH  BC
    PUSH  DE
    PUSH  HL
    LD    A,(BC)
    OR    A
    JR    Z,D3
    CP    144
    JR    C,D2
    LD    A,144
D2: LD    B,A
    CALL  0BFD0H
D3: POP   HL
    LD    DE,158
    ADD   HL,DE
    POP   DE
    INC   D
    POP   BC
    INC   BC
    LD    A,D
    CP    6
    JR    NZ,L8
    RET
    DB    0
