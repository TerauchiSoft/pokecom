org 0100h

overwrite:equ 0
x0:equ 30
xm:equ 140
y0:equ 0
ym:equ 47

grp:equ 0bfd0h
msp:equ 0bff1h
kwait:equ 0bcfdh
kscan:equ 0be53h



;*******************Title graph********************
start:
 call cls
 ld HL,graph_title
 ld DE,0008h
 ld B,50
 call grp_open
start_l:
 ld A,1
 ld (floor),A
 call keysc		;left,right,on,0,/,space,down,up
 bit 2,A
 ret nz
 bit 3,A
 jr nz,before_game_c
 bit 5,A
 jr nz,Stating_before
 jr start_l
;*****************Continue*******************
before_game_c:
 bit 5,A
 jr z,start_l
 call cls
 ld HL,continue_ms
 ld DE,0102h
 ld B,18
 call msp
 ld HL,(floor)
 ld H,0
 call ascn
before_game_c_floor:
 ld HL,(floor)
 ld H,0
 call ascn
 ld HL,nmemry+3
 ld DE,030bh
 ld B,2
 call msp
 call kwait
 ld HL,floor
 cp 1eh
 jr z,Stating_before
 cp 20h
 jr z,before_game_c_u
 cp 1fh
 jr nz,before_game_c_floor
before_game_c_d:
 dec (HL)
 xor A
 cp (HL)
 jr nz,before_game_c_floor
 inc (HL)
 jr before_game_c_floor
before_game_c_u:
 inc (HL)
 ld A,61
 cp (HL)
 jr nz,before_game_c_floor
 dec (HL)
 jr before_game_c_floor
;***************Starting game****************
Stating_before:
 call cls
 ld HL,stating_ms1
 ld DE,0108h
 ld B,9
 call msp
 ld HL,stating_ms2
 ld De,0208h
 ld B,10
 call msp
 ld HL,stating_msf
 ld De,0408h
 ld B,5
 call msp
 ld HL,(floor)
 ld H,0
 call ascn
 ld HL,nmemry+3
 ld DE,040eh
 ld B,2
 call msp
 call tim255
 call kwait


 ld c,2
 ld A,27
 ld (Stating_before_mkwall1+1),A
Stating_before_mkwall2
 ld D,0
 ld B,6
Stating_before_mkwall1:
 ld E,overwrite
 push BC
 push DE
 ld HL,wall_grp
 ld B,3
 call grp_set+2
 pop DE
 pop BC
 ld A,8
 add A,D
 ld D,A
 djnz Stating_before_mkwall1
 ld A,141
 ld (Stating_before_mkwall1+1),A
 dec C
 jr nz,Stating_before_mkwall2

 ld c,15
 ld A,36
 ld (Stating_before_mkwall11+1),A
Stating_before_mkwall21
 ld D,6
 ld B,6
Stating_before_mkwall11:
 ld E,overwrite
 push BC
 push DE
 ld HL,pole_grp
 ld B,1
 call grp_set+2
 pop DE
 pop BC
 ld A,7
 add A,D
 ld D,A
 djnz Stating_before_mkwall11
 ld A,(Stating_before_mkwall11+1)
 add A,7
 ld (Stating_before_mkwall11+1),A
 dec C
 jr nz,Stating_before_mkwall21

stating_obj:
 ld HL,obj
 ld (HL),0
 ld DE,obj+1
 ld BC,47
 ldir

 ld HL,obj_1
 ld A,(fl)
 dec A
 jr Z,stating_obj_1

 ld D,A

stating_obj_2:
 ld BC,0
 ld A,0
 cpir
 dec D
 jr nz,stating_obj_2
stating_obj_1:
 ld DE,obj
stating_obj_3:
 ld A,(HL)
 or A
 jr z,stating_obj_4
 ldi
 jr stating_obj_3
stating_obj_4:
 
;wall

 call grp_o

 jr main
stating_ms1:
 db "GET READY"
stating_ms2:
 db "PLAYER ONE"
stating_msf:
 db "FLOOR"
continue_ms:
 db "SELECT START FLOOR"
wall_grp:
 db 0ffh,0ffh,0ffh
pole_grp:
 db 1h
;**************Main****************
main:
 ld HL,obj
 ld DE,-1
main_l:
 inc DE
 xor A
 or (HL)
 jr z,main
 bit 4,(HL)

 bit 5,(HL)

 bit 6,(HL)

 bit 7,(HL)
;sraim
 cp 2
 jp c,Gsraim
 jp z,Bsraim
 cp 4
 jp c,Rsraim
 jp z,Busraim
 cp 6
 jp c,Dsraim
 jp z,DYsraim

 jp Gsraim
Gsraim:
 ld HL,obj_hp
 add HL,DE
 xor A
 cp (HL)
 jp z,main_l













 ret









































;*********************************************************
TIMBTL:
 LD A,50
 JR TIMER
TIM30:
 LD A,37
 JR TIMER
TIM255:
 LD A,255
TIMER:
 LD BC,0
 INC BC;LOOP
 DB 0,0,0,0,0,0
 XOR B
 RET Z
 JR TIMER+3
XSAN:
 OR A
 RET Z
XSANL:
 ADD HL,DE
 DEC A
 JR NZ,XSANL
 RET
NMEMRY:
 DB 0,0,0,0,0
ASCN:;HL=���l(2byte)��DE�̱��ڽ
 LD DE,NMEMRY
 LD BC,10000
 CALL WARIS
 LD BC,1000
 CALL WARIS
 LD BC,100
 CALL WARIS
 LD BC,10
 CALL WARIS
 LD A,L
 LD (DE),A
 LD B,5
 LD DE,NMEMRY
ASCL:
 LD A,(DE)
 CP 0
 JR Z,ASCL2
 JR ASC2
ASCL2:
 INC DE
 DJNZ ASCL
 LD B,1
 LD DE,NMEMRY+4
ASC2:
 LD A,(DE)
 LD C,30H
 ADD A,C
 LD (DE),A
 INC DE
 DJNZ ASC2
 RET
WARIS:
 XOR A
;WARI1:
 SBC HL,BC
 JR C,WARI2
 INC A
 JR WARIS+1
WARI2:
 ADD HL,BC
 LD (DE),A
 INC DE		;
 RET
cls:
 push AF
 push DE
 push HL
 push BC
 ld A,0
 ld HL,grapu_area
 ld DE,grapu_area+1
 ld (HL),A
 ld BC,144*6-1
 ldir
 call grp_o
 pop BC
 pop HL
 pop DE
 pop AF
 ret

bls:
 push AF
 push DE
 push HL
 push BC
 LD HL,grapu_area
 LD (HL),0FFH
 LD DE,grapu_area+1
 LD BC,863
 LDIR
 CALL grp_o
 EXX
 pop BC
 pop HL
 pop DE
 pop AF
 RET
grp_o:
 ld A,6
 ld DE,0
 ld HL,grapu_area
grp_o_l:
 push DE
 push AF
 ld B,144
 call grp
 inc HL
 pop AF
 pop DE
 inc D
 dec A
 jr nz,grp_o_l
 ret

grp_open:
 ld C,6
grp_open_l
 push BC
 push DE
 call grp
 pop DE
 inc D
 inc HL
 pop BC
 dec C
 jr nz,grp_open_l
 ret

keysc:
 di
 xor A
 out (11h),A
 ld A,08h
 out (11h),A
 call 8aadh
 in A,(10h)
 and 0e0h
 ld B,A
 xor A
 out (11h),A
 ld A,10h
 out (11h),A
 call 8aadh
 in A,(10h)
 and 0fh
 or B
 ei
 ret

grp_set:;HL_data,DE_(y,x),B_databit
 ld B,6

 ld A,E
 ld (grp_set_or1+1),A
 ld A,D
 ld D,0
grp_set_set1:
 inc D
 sub 8
 jr nc,grp_set_set1
 dec D
 add A,8
grp_set_set2:
 ld (grp_set_shiftd_bit+1),A
 ld C,A
 ld A,8
 sub C
 ld (grp_set_shiftu_bit+1),A

grp_set_shiftd:
 ld A,D			;line number
 push HL					;HL
 ld HL,grapu_area
 ld DE,144
 call xsan		;y+
grp_set_or1:
 ld E,overwrite
 ld D,0
 add HL,DE		;x+
 ex DE,HL
 pop HL			;data
 push HL					;HL
 push BC						;BC
grp_set_shiftd_bit:
 ld C,overwrite		;shift bit count
 ld A,(HL)
 ex AF,AF'
 ld A,C
 or A
 jr z,grp_set_shiftd_bit_2
 ex AF,AF'
grp_set_shiftd_bit_l:
 sla A
 dec C
 jr nz,grp_set_shiftd_bit_l
 ex AF,AF'
grp_set_shiftd_bit_2:
 ex AF,AF'
 ld C,A
 ld A,(DE)
grp_set_change1:
 or C
 ld (DE),A
grp_set_shiftd_l:
 inc HL
 inc DE
 djnz grp_set_shiftd_bit

 dec DE

 ex DE,HL
 ld DE,144
 add HL,DE


 pop BC						;HL
 ld A,L
 sub B
 jr nc,grp_set_shiftu_bit_b
 dec H
grp_set_shiftu_bit_b:
 ld L,A
 ex DE,HL

 pop HL			;data2
grp_set_shiftu_bit:
 ld C,overwrite		;shift bit count
 ld A,(HL)
grp_set_shiftu_bit_l
 srl A
 dec C
 jr nz,grp_set_shiftu_bit_l
 ld C,A
 ld A,(DE)
grp_set_change2:
 or C
 ld (DE),A
grp_set_shiftu_l:
 inc HL
 inc DE
 djnz grp_set_shiftu_bit

; call grp_o
 ret


grp_del:;HL_data,DE_(y,x),B_databit
 ld HL,del_graph
 ld B,6
 ld A,E
 ld (grp_set_or1d+1),A
 ld A,D
 ld D,0
grp_set_set1d:
 inc D
 sub 8
 jr nc,grp_set_set1d
 dec D
 add A,8
grp_set_set2d:
 ld (grp_set_shiftd_bitd+1),A
 ld C,A
 ld A,8
 sub C
 ld (grp_set_shiftu_bitd+1),A

grp_set_shiftdd:
 ld A,D			;line number
 push HL					;HL
 ld HL,grapu_area
 ld DE,144
 call xsan		;y+
grp_set_or1d:
 ld E,overwrite
 ld D,0
 add HL,DE		;x+
 ex DE,HL
 pop HL			;data
 push HL					;HL
 push BC						;BC
grp_set_shiftd_bitd:
 ld C,overwrite		;shift bit count
 ld A,(HL)
 ex AF,AF'
 ld A,C
 or A
 jr z,grp_set_shiftd_bit_2d
 ex AF,AF'
grp_set_shiftd_bit_ld:
 scf
 rla
 dec C
 jr nz,grp_set_shiftd_bit_ld
 ex AF,AF'
grp_set_shiftd_bit_2d:
 ex AF,AF'
 ld C,A
 ld A,(DE)
grp_set_change1d:
 and C
 ld (DE),A
grp_set_shiftd_ld:
 inc HL
 inc DE
 djnz grp_set_shiftd_bitd

 dec DE

 ex DE,HL
 ld DE,144
 add HL,DE


 pop BC						;HL
 ld A,L
 sub B
 jr nc,grp_set_shiftu_bit_bd
 dec H
grp_set_shiftu_bit_bd:
 ld L,A
 ex DE,HL

 pop HL			;data2
grp_set_shiftu_bitd:
 ld C,overwrite		;shift bit count
 ld A,(HL)
grp_set_shiftu_bit_ld
 scf
 rra
 dec C
 jr nz,grp_set_shiftu_bit_ld
 ld C,A
 ld A,(DE)
grp_set_change2d:
 and C
 ld (DE),A
grp_set_shiftu_ld:
 inc HL
 inc DE
 djnz grp_set_shiftu_bitd
 ret

del_graph:
 db 0c0h,0c0h,0c0h,0c0h,0c0h,0c0h
 ds 144
grapu_area:
 ds 144*6
 ds 144
;********************************Graphics**********************************
graph_title:
 db 00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,80h,60h,80h,00,00,0C0h,30h,0Ch,08h,30h,0C0h,0C0h,30h,08h,10h,0E0h,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
 db 00,00,00,00,00,00,00,00,00,80h,0C0h,00,00,80h,60h,10h,18h,06h,01h,00h,01h,0Eh,03h,00,00,00,00,00,00,01,00,00,00,03,0Ch,30h,40h,080h,00,80h,0E0h,00,00,00,00,00,00,00,00,00
 db 00,00,00,00,00,00,0C0h,30h,0Eh,01h,4Fh,66h,0F1h,0E0h,0F0h,58h,58h,40h,20h,80h,0E0h,0F8h,80h,0F0h,0C0h,0E0h,0F0h,0D8h,68h,0A9h,0C3h,42h,82h,86h,84h,0Ch,18h,79h,0F6h,0E1h,0C1h,86h,38h,0C0h,00,00,00,00,00,00
 db 00,80h,40h,20h,18h,06h,01h,00,00,00,00,0Fh,3Fh,7Fh,0FEh,0F8h,0F8h,0F0h,0FEh,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,0FFh,7Eh,7Eh,7Dh,7Dh,78h,7Dh,3Fh,1Fh,0Fh,00,00,03h,0Ch,70h,80h,00,00
 db 00,0FFh,00,00,00,00,00,00,00,0F8h,08,08,0F0h,00,0FCh,02Eh,06Fh,0D7h,07,7Fh,80h,00,0F8h,00,0F0h,50h,4Ch,4Eh,0FFh,07,0F7h,0Fh,4Eh,4Ch,0D4h,00,0F0h,50h,48h,48h,0F8h,00,00,00,00,00,00,0Fh,30h,0C0h
 db 78h,47h,40h,40h,40h,40h,44h,44h,44h,45h,45h,45h,44h,44h,45h,44h,44h,45h,44h,44h,45h,45h,45h,44h,45h,44h,44h,44h,45h,44h,44h,45h,45h,45h,45h,44h,45h,44h,44h,44h,45h,44h,44h,44h,40h,40h,40h,40h,40h,7Fh
grapu_gil_r:
 db 0ffh,0ffh,0ffh,0ffh,0ffh,0ffh
 db 00,0Ch,3Fh,17h,0Ch,00
;************************************Data**************************************
obj_1:db 1,1,1,1,1,83h,84h,85h,8fh,0
;**********************************Variable**************************************
floor:db 0
obj:ds 48
obj_hp:ds 48
obj_xy:ds 48
obj_wp:ds 48
obj_ap:ds 48


end